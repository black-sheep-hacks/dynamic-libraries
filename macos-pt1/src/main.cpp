#include <iostream>
#include "mymath.h"

void myFunction() {
    int a, b;
    a = 55;
    b = 66;
    std::cout << "My math adds: " << mymath::add(a, b) << std::endl;
    std::cout << "My math subtracts: " << mymath::sub(a, b) << std::endl;
}

int main() {
    std::cout << "Hello Easy C++ project!" << std::endl;
    myFunction();
    return 0;
}