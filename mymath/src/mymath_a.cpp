#include <iostream>

#define EXPORT __attribute__((visibility("default")))

__attribute__((constructor))
static void _constructor() {
	std::cout << "Initializing mymath\n";
}

EXPORT
extern "C" int add(int a, int b) {
	return a + b;
}
EXPORT
extern "C" int sub(int a, int b) {
	return a - b;
}

__attribute__((destructor))
static void _destructor() {
	std::cout << "Destroying mymath\n";
}
