#include <iostream>
#include "mymath.h"

#define EXPORT __attribute__((visibility("default")))

__attribute__((constructor))
static void _constructor() {
	std::cout << "Initializing mymath\n";
}

EXPORT
int mymath::add(int a, int b) {
	return a + b;
}
EXPORT
int mymath::sub(int a, int b) {
	return a - b;
}

__attribute__((destructor))
static void _destructor() {
	std::cout << "Destroying mymath\n";
}
