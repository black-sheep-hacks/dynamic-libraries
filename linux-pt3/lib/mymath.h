extern "C" void init() __attribute__((constructor));
extern "C" void destroy() __attribute__((destructor));
extern "C" int add(int, int);
extern "C" int sub(int, int);

