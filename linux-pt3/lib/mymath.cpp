#include <iostream>
#include "mymath.h"

void init() {
	printf("Initializing mymath\n");
}

void destroy(){
	printf("Destroying mymath\n");
}

int add(int x, int y){
	std::cout << "Adding\n";
	return x + y;
}

int sub(int x, int y){
	std::cout << "Subtracting\n";
	return x - y;
}

