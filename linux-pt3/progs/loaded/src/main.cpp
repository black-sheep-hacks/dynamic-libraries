#include <iostream>
#include <dlfcn.h>

int main() {
	void* lib_handle;
	int (*add)(int, int);
	int (*sub)(int, int);

	lib_handle = dlopen("/usr/local/lib/libmymath.so", RTLD_LAZY);
	if(!lib_handle) {
		std::cout << "Did not find libmymath\n";
		return 1;
	}
	add = (int(*)(int, int))dlsym(lib_handle, "add");
	sub = (int(*)(int, int))dlsym(lib_handle, "sub");
	if(!add || !sub){
		std::cerr << "Could not load functions\n";
		return 2;
	}
	std::cout << "adding 3+4=" << (*add)(3, 4) << std::endl;
	std::cout << "subtracting 3-4=" << (*sub)(3, 4) << std::endl;
	dlclose(lib_handle);
	return 0;
}

