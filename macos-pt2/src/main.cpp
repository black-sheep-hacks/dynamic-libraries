#include <iostream>
#include <cstring>
#include <dlfcn.h>

int main(int argc, char** argv) {
    const char* libName = "bin/mymath.A.dylib";
    void *libHandle;
    std::cout << "# main() starts" << std::endl;
    libHandle = dlopen(libName, RTLD_NOW);
    if(libHandle == NULL) {
        std::cerr << "Error opening mymath:\n" << dlerror() << std::endl;;
        return 1;
    }
    int (*mymath_add)(int, int) = (int(*)(int, int))dlsym(libHandle, "add");
    if(mymath_add == NULL) {
        std::cerr << "Error opening while getting address of add:\n" << dlerror() << std::endl;;
        return 2;
    }
    int (*mymath_sub)(int, int) = (int(*)(int, int))dlsym(libHandle, "sub");
    if(mymath_sub == NULL) {
        std::cerr << "Error opening while getting address of add:\n" << dlerror() << std::endl;;
        return 2;
    }
    std::cout << "# testing add: 55+66=" << mymath_add(55, 66) << std::endl;
    std::cout << "# testing subtract: 55-66=" << mymath_sub(55, 66) << std::endl;
    if(dlclose(libHandle) != 0) {
        std::cerr << "Error closing library:\n" << dlerror() << std::endl;;
        return 2;
    }
    std::cout << "# main() exits" << std::endl;
    if(argc == 2 && strncmp(argv[1], "crash\n", strlen("crash\n"))){
        std::cout << "# testing subtract: 55-66=" << mymath_sub(55, 66) << std::endl;
    }
    return 0;
}
